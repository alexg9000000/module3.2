﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Module3_2
{
    static public class Program
    {
        static void Main(string[] args)
        {
            Task4 task4 = new Task4();
            Console.Write("Введите натуральное число: ");
            string n = Console.ReadLine();
            int number;
            if (task4.TryParseNaturalNumber(n, out number))
            {
                Console.WriteLine(task4.GetFibonacciSequence(number));
            }
            else
            {
                Console.WriteLine("Число не является натуральным");
            }
            Task5 task5 = new Task5();
            Console.Write("Введите число для реверсирования: ");
            int value = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(task5.ReverseNumber(value));
            Task6 task6 = new Task6();
            Console.Write("Введите размер массива: ");
            int size1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(task6.UpdateElementToOppositeSign(task6.GenerateArray(size1)));
            Task7 task7 = new Task7();
            Console.Write("Введите размер массива: ");
            int size2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(task7.FindElementGreaterThenPrevious(task7.GenerateArray(size2)));
            Task8 task8 = new Task8();
            Console.Write("Введите размер массива: ");
            int size = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(task8.FillArrayInSpiral(size));
        }
    }
    public class Task4
    {
        public bool TryParseNaturalNumber(string input, out int result)
        {
            var value = int.TryParse(input, out result);
            if (!value)
            {
                return false;
            }
            else
            {
                if (result >= 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public int[] GetFibonacciSequence(int n)
        {
            int[] fib = new int[n];
            if(n == 1)
            {
                fib[0] = 0;
            }
            if(n == 2)
            {
                fib[1] = 1;
            }
            for (int i = 2; i < fib.Length; i++)
            {
                fib[0] = 0;
                fib[1] = 1;
                fib[i] = fib[i - 1] + fib[i - 2];
            }
            return fib;
        }
    }
    public class Task5
    {
        public int ReverseNumber(int sourceNumber)
        {
            string word = Convert.ToString(sourceNumber);
            StringBuilder str = new StringBuilder(word.Length);
            if (sourceNumber < 0)
            {
                for (int i = word.Length; i-- != 1;)
                {
                    str.Append(word[i]);
                }
                string value = word[0] + str.ToString();
                int reverse = Convert.ToInt32(value);
                return reverse;
            }
            else
            {
                for (int i = word.Length; i-- != 0;)
                {
                    str.Append(word[i]);
                }
                string value = str.ToString();
                int reverse = Convert.ToInt32(value);
                return reverse;
            }
        }
    }
    public class Task6
    {
        public int[] GenerateArray(int size)
        {
            int[] array = null;
            Random random = new Random();
            if (size < 0)
            {
                return array;
            }
            else
            {
                array = new int[size];
                for (int i = 0; i < array.Length; i++)
                {
                    array[i] = random.Next(-10, 10);
                }
                return array;
            }
        }
        public int[] UpdateElementToOppositeSign(int[] source)
        {
            int[] genArray = null;
            if(source == null)
            {
                return genArray;
            }
            else
            {
                genArray = source;
                for (int i = 0; i < genArray.Length; i++)
                {
                    genArray[i] = -source[i];
                }
                return genArray;
            }
        }
    }
    public class Task7
    {
        readonly List<int> list = new List<int>();
        public int[] GenerateArray(int size)
        {
            int[] array = null;
            Random random = new Random();
            if (size < 0)
            {
                return array;
            }
            else
            {
                array = new int[size];
                for (int i = 0; i < array.Length; i++)
                {
                    array[i] = random.Next(-10, 10);
                }
                return array;
            }
        }
        public List<int> FindElementGreaterThenPrevious(int[] source)
        {
            for (int i = 1; i < source.Length; i++)
            {
                if (source[i] > source[i - 1])
                {
                    list.Add(source[i]);
                }
            }
            return list;
        }
    }
    public class Task8
    {
        public int[,] FillArrayInSpiral(int size)
        {
            int[,] array = new int[size, size];
            int minRow = 0, maxRow = size - 1, minCol = 0, maxCol = size - 1;
            int count = 0;
            int max = size * size;
            while (count < max)
            {
                for (int i = minCol; i <= maxCol; i++)
                {
                    count++;
                    array[minRow, i] = count;
                }
                minRow = minRow + 1;
                for (int i = minRow; i <= maxRow; i++)
                {
                    count++;
                    array[i, maxCol] = count;
                }
                maxCol = maxCol - 1;
                for (int i = maxCol; i >= minCol; i--)
                {
                    count++;
                    array[maxRow, i] = count;
                }
                maxRow = maxRow - 1;
                for (int i = maxRow; i >= minRow; i--)
                {
                    count++;
                    array[i, minCol] = count;
                }
                minCol = minCol + 1;
            }
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    Console.Write("{0}, ", array[i, j]);
                }
                Console.WriteLine();
            }
            return array;
        }
    }
}